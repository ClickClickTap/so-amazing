# so-amazing

## Updating Your Page

When you're ready to install the latest version of your template, these are the steps you should follow:

1.  BACKUP YOUR EXISTING PAGE.  There are going to be problems, there will be mistakes.  A backup is the best insurance and also reduces anxiety.  (Just so we're clear, it's my anxiety we're talking about here.  :) )

2. Visit my gitlab account for the latest version of your repository.  Go to the following URL:

[https://gitlab.com/ClickClickTap/so-amazing/-/blob/main/val.html](https://gitlab.com/ClickClickTap/so-amazing/-/blob/main/val.html)

If you're ready to try the version that has the image sidebar, you can get that here:

[https://gitlab.com/ClickClickTap/so-amazing/-/blob/main/val-sidebar.html](https://gitlab.com/ClickClickTap/so-amazing/-/blob/main/val-sidebar.html)

3.  You should see the gitlab page for a file titled `val.html`

4.  A little ways down on the page is a row of buttons.  Among them are buttons for "Open in Web IDE", then "Replace", and then "Delete."  To the right of those three is a tiny button that, when you hover the cursor over it, will tell you it will "copy file contents" to your clipboard.  Click that button.

5.  Go to your cb page and open your profile.  Click "Edit your bio." 

6.  Click inside the big edit window for "About Me".

7.  Press cmd+a and then cmd+v  

8.  That should have pasted all this new code over top of your old code.

9.  When you're sure the changes are what you want them to be, click "Update bio" at the bottom of the page.

10.  Scroll up to review your page.

11.  Visit https://chaturbate.com/valerie_james3 to view your page in the manner that your fans will see it.  (It looks different when you're in edit mode.  In particular, the spacing is different.)

12.  View your page in both light mode and dark mode.

13.  Return to the editor to update the calendar.  (For the first release, I updated the code to have your current calendar.  But please have a look at the code and let me know if it's clear to you how you can change it.)

That should do it.

[Handy list of pink colors](https://html-color.codes/pink)
